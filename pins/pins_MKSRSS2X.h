/**
 * MKS gen 1.4  with RAMPS v1.4 pin base assignments
 * Mk I dual chinerma extruder, one autofan, one blower
 * and zprobe (single z drive)
 *
 * + z probe using servo pin 5
 *
 * + tinyfans on pin 6 and 11
 *    blower = pin 6
 *    autofan = 11 shared
 */

#define IS_RAMPS_EFB
//
// bed = 8, extruder0 = 10, extruder1 = 7, all builtin FETS on MKS1.4
// tiny fan running autofans on pin 11 and blower on 6
// FET on D9 could be used for another power application eg extruder2?
//
#include "pins_RAMPS_14.h"

#ifdef Z_MIN_PROBE_PIN
#undef Z_MIN_PROBE_PIN
#endif
#define Z_MIN_PROBE_PIN    5  //  servo pin 5 with +5 V enabled
//
// @section machine mks gen 1.4 has stepper motor
//   sockets reversed from MPX3 board/RAMPS
//
// Invert the stepper directions. Change (or reverse the motor connector)
//  if an axis goes the wrong way.
//
#ifdef INVERT_X_DIR
#undef INVERT_X_DIR
#endif
#ifdef INVERT_Y_DIR
#undef INVERT_Y_DIR
#endif
#ifdef INVERT_Z_DIR
#undef INVERT_Z_DIR
#endif

// x is inverted due to lower edge clamp, bias to balance nozzle drag

#define INVERT_X_DIR true
#define INVERT_Y_DIR true
#define INVERT_Z_DIR false

// @section extruder

#ifdef INVERT_E0_DIR
#undef INVERT_E0_DIR
#endif
#ifdef INVERT_E1_DIR
#undef INVERT_E1_DIR
#endif
#ifdef INVERT_E2_DIR
#undef INVERT_E2_DIR
#endif
#ifdef INVERT_E3_DIR
#undef INVERT_E3_DIR
#endif

// For direct drive extruder v9 set to true, for geared extruder set to false.
#define INVERT_E0_DIR true
#define INVERT_E1_DIR true
#define INVERT_E2_DIR true
#define INVERT_E3_DIR true
//
// override  Extruder cooling fans and enable whether or not in config advanced
// Configure fan pin outputs to automatically turn on/off when the associated
// extruder temperature is above/below EXTRUDER_AUTO_FAN_TEMPERATURE.
// Multiple extruders can be assigned to the same pin in which case
// the fan will turn on when any selected extruder is above the threshold.
//
#ifdef EXTRUDER_0_AUTO_FAN_PIN
#undef EXTRUDER_0_AUTO_FAN_PIN
#endif
#ifdef EXTRUDER_1_AUTO_FAN_PIN
#undef EXTRUDER_1_AUTO_FAN_PIN
#endif
#ifdef EXTRUDER_2_AUTO_FAN_PIN
#undef EXTRUDER_2_AUTO_FAN_PIN
#endif
#ifdef EXTRUDER_3_AUTO_FAN_PIN
#undef EXTRUDER_3_AUTO_FAN_PIN
#endif

#define EXTRUDER_0_AUTO_FAN_PIN  11
#define EXTRUDER_1_AUTO_FAN_PIN  11
#define EXTRUDER_2_AUTO_FAN_PIN -1
#define EXTRUDER_3_AUTO_FAN_PIN -1

#ifdef EXTRUDER_AUTO_FAN_TEMPERATURE
#undef EXTRUDER_AUTO_FAN_TEMPERATURE
#endif
#ifdef EXTRUDER_AUTO_FAN_SPEED
#undef EXTRUDER_AUTO_FAN_SPEED
#endif

#define EXTRUDER_AUTO_FAN_TEMPERATURE 50
#define EXTRUDER_AUTO_FAN_SPEED   255  // == full speed

