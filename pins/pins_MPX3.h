/**
 * MPX-3 with RAMPS v1.4 pin assignments EEB + fan
 *
 *  ie RAMPS_14_EBFE (Extruder, Bed, Fan, Extruder)
 */
//#define IS_RAMPS_EBFE
#define IS_RAMPS_EEB

#include "pins_RAMPS_14.h"

#undef HEATER_BED_PIN
#define HEATER_BED_PIN    8    // BED - 8/-1

#undef FAN_PIN
#define FAN_PIN           9    // FAN

#undef HEATER_0_PIN
#define HEATER_0_PIN     10    // E0

#undef HEATER_1_PIN
#define HEATER_1_PIN      7    // E1

#undef Z_MIN_PROBE_PIN
#define Z_MIN_PROBE_PIN   11   // servo pin I use for zprobe

